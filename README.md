<div align="center">

![](/assets/logo_compressed.png "docker-rails")

<br>

![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white) ![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white) ![Ruby](https://img.shields.io/badge/ruby-%23CC342D.svg?style=for-the-badge&logo=ruby&logoColor=white) ![Rails](https://img.shields.io/badge/rails-%23CC0000.svg?style=for-the-badge&logo=ruby-on-rails&logoColor=white) ![NodeJS](https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white) ![NPM](https://img.shields.io/badge/NPM-%23000000.svg?style=for-the-badge&logo=npm&logoColor=white) ![Yarn](https://img.shields.io/badge/yarn-%232C8EBB.svg?style=for-the-badge&logo=yarn&logoColor=white)

<br>
<a href="#"><img src="https://img.shields.io/badge/ubuntu-20.04-red"></a>
<a href="#"><img src="https://img.shields.io/badge/ruby-2.6.0-red"></a>
<a href="#"><img src="https://img.shields.io/badge/rails-6.0.0-red"></a>
<a href="https://paypal.me/rodrigoodhin"><img src="https://img.shields.io/badge/donate-PayPal-blue"></a>
<br><br>
</div>

# Docker + Ruby On Rails

A Docker image with a linux environment prepared with the the main features for develop in Ruby On Rails.

&nbsp;
&nbsp;
&nbsp;

## Download image from Docker Hub

```shell
docker pull rodrigoodhin/rails
```

&nbsp;
&nbsp;
&nbsp;

## Available tags

| Tag    | Image                     | Ruby Version | Rails Version |
| ------ | ------------------------- | ------------ | ------------- |
| latest | rodrigoodhin/rails:latest | 2.6.0        | 6.0.0         |
| 6.0.0  | rodrigoodhin/rails:6.0.0  | 2.6.0        | 6.0.0         |
| 5.2.0  | rodrigoodhin/rails:5.2.0  | 2.5.8        | 5.2.0         |

&nbsp;
&nbsp;
&nbsp;

## Create a new image

```shell
docker build -t <YOUR_DOCKER_HUB_USER>/rails .
```

&nbsp;
&nbsp;
&nbsp;

## Push created image to docker hub

```shell
docker login
docker push <YOUR_DOCKER_HUB_USER>/rails
```

&nbsp;
&nbsp;
&nbsp;

## Using docker-compose.yml

_To use this image within your project, copy the file `docker-compose.yml` to your project's folder and execute the steps below._

#### Start services

```shell
docker-compose up -d
```

#### List containers to get SSH port

```shell
docker container ls
```

#### Open SSH connection

- _The password is: app_

```shell
ssh app@localhost -p <SSH_PORT>
```

&nbsp;
&nbsp;
&nbsp;

## LICENSE

[MIT License](/LICENSE)
